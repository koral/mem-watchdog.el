;;; mem-watchdog.el --- -*- lexical-binding: t -*-

;;; Commentary:

;; Look after the memory usage of a program and trap into it with gud
;; if exceeds a given threshold.

(require 'rx)

(defcustom mw-pid 32485
  "PID to look for.")

(defcustom mw-mem-threshold 50
  "Percentage relative to system availability.")

(defcustom mw-program
  "~/emacs/src/emacs"
  "Executable to look for.")

(defcustom mw-interval 5
  "Intervals between two probes in seconds.")

(defun mw-regexp ()
  "Return the regexp to match against ps output."
  (rx-to-string `(seq bol ,(number-to-string mw-pid) space
		      (1+ (any "0-9a-z_")) (1+ space)
		      (group-n 1 (1+ num) "." (1+ num)))))

(defvar mw-timer nil)

(defun mw-check ()
  (with-current-buffer (get-buffer-create "*Emacs memory watchdog*")
    (erase-buffer)
    (call-process "ps" nil t nil "-o" "pid,user,%mem,command" "ax")
    (goto-char (point-min))
    (re-search-forward (mw-regexp))
    (when (> (string-to-number (match-string 1))
	   mw-mem-threshold)
	(cancel-timer mw-timer)
	(gud-gdb (concat "gdb "(expand-file-name mw-program)
			 " --fullname " (number-to-string mw-pid))))))

(defun mw-start ()
  "Start running the memory watchdog."
  (interactive)
  (setf mw-timer (run-with-timer 0 mw-interval #'mw-check)))
